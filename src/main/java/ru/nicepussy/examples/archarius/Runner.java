package ru.nicepussy.examples.archarius;

import com.netflix.config.ConfigurationManager;
import com.netflix.config.DynamicPropertyFactory;
import com.netflix.config.DynamicStringProperty;
import com.netflix.config.DynamicWatchedConfiguration;
import com.netflix.config.source.ZooKeeperConfigurationSource;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.Executors;

public class Runner {
  final static Logger LOGGER = LoggerFactory.getLogger(Runner.class);

  public static void main(String[] args)
  throws Exception {

    String zkConfigRootPath = "/testApp/config";
    String zkConnectionString = "localhost:2181";

    CuratorFramework client = CuratorFrameworkFactory.newClient(
        zkConnectionString,
        new ExponentialBackoffRetry(1000, 3));

    client.start();

    ZooKeeperConfigurationSource zkConfigSource = new ZooKeeperConfigurationSource(
        client,
        zkConfigRootPath);
    zkConfigSource.start();

    DynamicWatchedConfiguration zkDynamicConfig = new DynamicWatchedConfiguration(zkConfigSource);

    ConfigurationManager.install(zkDynamicConfig);

    final DynamicStringProperty stringProperty = DynamicPropertyFactory.getInstance()
        .getStringProperty("testProperty", "suchka");

    Executors.newSingleThreadExecutor().submit(() -> {
      final String s = stringProperty.get();
      System.out.println(s);
      stringProperty.addCallback(() -> {
        System.out.println(stringProperty.getValue());
      });
    });

  }
}
